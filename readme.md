## Requirements
python3
basic terminal/command line usage

# Python script to find the cheapest PGX rental for respective energies
After running the script, results will be recorded in pega_price.txt

## Instructions
```
pip install -r requirements.txt
python3 find_cheapest.py
```

