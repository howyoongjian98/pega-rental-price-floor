import requests
from pprint import pprint
from multiprocessing import Process, Lock, Manager
import json
from datetime import datetime, date

today = date.today()
now = datetime.now()

l= Lock()

pega_price_map = Manager().dict({
    0: float("inf"),
    1: float("inf"),
    2: float("inf"),
    3: float("inf"),
    4: float("inf"),
    5: float("inf"),
    6: float("inf"),
    7: float("inf"),
    8: float("inf"),
    9: float("inf"),
    10: float("inf"),
    11: float("inf"),
    12: float("inf"),
    13: float("inf"),
    14: float("inf"),
    15: float("inf"),
    16: float("inf"),
    17: float("inf"),
    18: float("inf"),
    19: float("inf"),
    20: float("inf"),
    21: float("inf"),
    22: float("inf"),
    23: float("inf"),
    24: float("inf"),
    25: float("inf"),
})

def update_pega_price(page):
    url = f"https://api-apollo.pegaxy.io/v1/game-api/rent/{page}?sortBy=price&sortType=ASC&rentMode=PAY_RENT_FEE"

    response = requests.get(url)
    if response.status_code >= 200 and response.status_code < 300:
        response = response.json()
    else:

        print(f'{response.status_code} error fetching results from page {page}')
        return
    print(f'tabulating results from page {page}')

    for res in response['renting']:
        price = float(res['price'])/1000000000000000000 
        pega_price_map[res['pega']['energy']] = min(price, pega_price_map[res['pega']['energy']])


jobs = []
# for page in range(1,20):
for page in range(1, 100):
    p = Process(target=update_pega_price,
                                args=(page,))
    p.start()
    jobs.append(p)

[job.join() for job in jobs]

pprint(pega_price_map._getvalue())

with open("pega_price.txt", "a") as file_object:
    # Append 'hello' at the end of file
    d2 = today.strftime("%B %d, %Y")
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    file_object.write(f'\n{d2} {current_time}\n')
    file_object.write(json.dumps(pega_price_map.copy(), indent=4))